using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;

namespace WorldCreator.Tiles
{
    public class QuadTilePlacer : MonoBehaviour
    {
        public QuadTileVisual Visual;
        public QuadTileVisual Next;
        public Vector3 NextStart;
        public Vector3 NextEnd;

        public float ToSphereDuration = 2f;
        public TileMapQuadSubdivide TileMap;

        void GetNext()
        {
            Next = Instantiate(Visual);

            Next.transform.position = NextStart;
            Next.transform.localScale = new Vector3(0,0,0);

            Next.transform.DOMove(NextEnd, 1f).SetEase(Ease.InOutBack);
            Next.transform.DOScale(Vector3.one * 0.5f, .5f).SetEase(Ease.OutQuint);
        }

        private void Start()
        {
            GetNext();
        }

        private void Update()
        {
            var mousePos = Mouse.current.position.ReadValue();
            var ray = Camera.main.ScreenPointToRay(new Vector3(mousePos.x, mousePos.y, 0));
            if (TileMap.Raycast(ray, out var tile, out var hit) && Mouse.current.leftButton.wasPressedThisFrame)
            {
                var newVisual = Next;
                newVisual.transform.SetParent(transform, true);
                newVisual.QuadTile = tile;
                
                DOTween.To(
                        () => newVisual.WarpAmount, 
                        value => newVisual.WarpAmount = value, 
                        1, 
                        ToSphereDuration)
                    .SetEase(Ease.InOutCubic);

                GetNext();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.black;
            Gizmos.DrawSphere(NextStart, 0.05f);
            Gizmos.color = Color.white;
            Gizmos.DrawSphere(NextEnd, 0.05f);
        }
    }
}