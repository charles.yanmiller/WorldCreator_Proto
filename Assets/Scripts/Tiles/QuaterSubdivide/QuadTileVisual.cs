using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEditor.ProBuilder;
using UnityEngine;
using UnityEngine.ProBuilder;

namespace WorldCreator.Tiles
{
    
    public class QuadTileVisual : MonoBehaviour
    {
        public ProBuilderMesh Mesh;
        public MeshRenderer MeshRenderer;
        private Material TileMaterial;
        public QuadTile QuadTile;
        [Range(0,1)] public float WarpAmount;
        
        //save all verts as barycentric coords within quad triangles.... then use this to map
        public float3 OriginP0;
        public float3 OriginP1;
        public float3 OriginP2;
        public float3 OriginP3;

        private void OnDrawGizmos()
        {
            float radius = 0.05f;
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(OriginP0, radius);
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(OriginP1, radius);
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(OriginP2, radius);
            Gizmos.color = new Color(1,0.5f,0);
            Gizmos.DrawSphere(OriginP3, radius);
        }

        [ContextMenu(nameof(AddWeights))]
        void AddWeights()
        {
            var verts = Mesh.GetVertices();
            List<Vector3> p = new List<Vector3>(verts.Length);
            
            foreach (var vert in verts)
            {
                p.Add(vert.position);
                Vector2 vertPosition2D = new Vector2(vert.position.x, vert.position.z);
                float magSqr = .3f;
                float distSqrP0 = Mathf.Abs(Vector2.Dot(OriginP0.xz, vertPosition2D));
                float distSqrP1 = Mathf.Abs(Vector2.Dot(OriginP1.xz, vertPosition2D));
                float distSqrP2 = Mathf.Abs(Vector2.Dot(OriginP2.xz, vertPosition2D));
                float distSqrP3 = Mathf.Abs(Vector2.Dot(OriginP3.xz, vertPosition2D));
                
                if (distSqrP0 < magSqr)
                {
                    vert.color = new Vector4(1, 0, 0, 0);
                    continue;
                }
                if (distSqrP1 < magSqr)
                {
                    vert.color = new Vector4(0, 1, 0, 0);
                    continue;
                }
                if (distSqrP2 < magSqr)
                {
                    vert.color = new Vector4(0, 0, 1, 0);
                    continue;
                }
                if (distSqrP3 < magSqr)
                {
                    vert.color = new Vector4(0, 0, 0, 1);
                    continue;
                }
                vert.color = new Vector4(1, 1, 1, 1).normalized;
            }
            Debug.Log(verts.Length);

            Mesh.SetVertices(verts);
            Mesh.RefreshUV(Mesh.faces);
            Mesh.RebuildWithPositionsAndFaces(p, Mesh.faces);
            Mesh.Optimize();
            Mesh.Refresh();
        }

        private void Start()
        {
            TileMaterial = MeshRenderer.material;
        }

        private void Update()
        {
            TileMaterial.SetFloat("_WarpAmount", WarpAmount);
            TileMaterial.SetVector("_P0", new Vector4(QuadTile.P0.x, QuadTile.P0.y, QuadTile.P0.z, 0));
            TileMaterial.SetVector("_P1", new Vector4(QuadTile.P1.x, QuadTile.P1.y, QuadTile.P1.z, 0));
            TileMaterial.SetVector("_P2", new Vector4(QuadTile.P2.x, QuadTile.P2.y, QuadTile.P2.z, 0));
            TileMaterial.SetVector("_P3", new Vector4(QuadTile.P3.x, QuadTile.P3.y, QuadTile.P3.z, 0));
        }
    }
}