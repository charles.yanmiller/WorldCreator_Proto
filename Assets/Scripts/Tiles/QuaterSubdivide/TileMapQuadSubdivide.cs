using System;
using System.Collections.Generic;
using UnityEditor.ProBuilder;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.ProBuilder;
using Random = UnityEngine.Random;

namespace WorldCreator.Tiles
{
    //https://docs.unity3d.com/Packages/com.unity.probuilder@4.2/manual/api.html
    public class TileMapQuadSubdivide : MonoBehaviour
    {
        [SerializeField] ProBuilderMesh probuilderMeshSource;
        [SerializeField] ProBuilderMesh probuilderMeshResult;
        [SerializeField] float Radius = 1;
        [SerializeField] public bool DebugDraw;

        public List<QuadTile> QuadTiles = new List<QuadTile>();

        [ContextMenu(nameof(BloatToSphere))]
        public void BloatToSphere()
        {
            {
                DestroyImmediate(probuilderMeshResult);
                probuilderMeshResult = Instantiate(probuilderMeshSource);
                probuilderMeshResult.gameObject.SetActive(true);
                probuilderMeshResult.name = "Sphere from cube";

                var verts = probuilderMeshResult.GetVertices();

                List<Vector3> positions = new List<Vector3>(verts.Length);
                for (int i = 0; i < verts.Length; i++)
                {
                    var vert = verts[i];

                    positions.Add(Vector3.Normalize(vert.position) * Radius);
                }

                probuilderMeshResult.RebuildWithPositionsAndFaces(positions, probuilderMeshResult.faces);
                probuilderMeshResult.Optimize(false);
                probuilderMeshResult.Refresh();
            }

            {
                var faces = probuilderMeshResult.faces;
                var verts = probuilderMeshResult.GetVertices();
                QuadTiles = new List<QuadTile>(probuilderMeshResult.faces.Count);
                foreach (var face in faces)
                {
                    if (face.distinctIndexes.Count != 4)
                    {
                        Debug.LogError($"face has {face.indexes.Count} when 4 is expected!");
                        continue;
                    }

                    //todo copy paste this func but make it non-gc allocating
                    var quad = face.ToQuad();
                    var v0 = verts[quad[0]].position;
                    var v1 = verts[quad[1]].position;
                    var v2 = verts[quad[2]].position;
                    var v3 = verts[quad[3]].position;
                    
                    if (v0 == v3 || v0 == v1 || v0 == v2 || v1 == v2 || v1 == v3 || v2 == v3)
                    {
                        Debug.LogWarning($"Face {face} has verts with duplicate values");
                    }
                    
                    QuadTiles.Add(new QuadTile(v0,v1,v2,v3));
                }
            }
            //todo highlight
            //todo mesh morphing
            //todo tile examples
            //todo connectivity
        }

        private void Update()
        {
            var mousePos = Mouse.current.position.ReadValue();
            var ray = Camera.main.ScreenPointToRay(new Vector3(mousePos.x, mousePos.y, 0));
            if (Raycast(ray, out var tile, out var hit))
            {
                if (DebugDraw)
                {
                    Debug.DrawLine(tile.P0, tile.P1);
                    Debug.DrawLine(tile.P1, tile.P2);
                    Debug.DrawLine(tile.P2, tile.P3);
                    Debug.DrawLine(tile.P3, tile.P0);

                    Debug.DrawLine(tile.P0, hit.Position);
                    Debug.DrawLine(tile.P1, hit.Position);
                    Debug.DrawLine(tile.P2, hit.Position);
                    Debug.DrawLine(tile.P3, hit.Position);
                }
            }
        }

        public bool Raycast(Ray ray, out QuadTile hitTile, out QuadTile.Hit hit)
        {
            hitTile = new QuadTile();
            hit = new QuadTile.Hit();
            
            if (QuadTiles == null)
                return false;
            
            float closestTile = Mathf.Infinity;
            bool hasHitSomething = false;
            for (int i = 0; i < QuadTiles.Count; i++)
            {
                var currentTile = QuadTiles[i];
                if (currentTile.Intersect(ray, out QuadTile.Hit potentialHit))
                {
                    hasHitSomething = true;
                    hit = potentialHit;
                    if (hit.Distance < closestTile)
                    {
                        hitTile = currentTile;
                        closestTile = hit.Distance;
                    }
                }
            }
            
            return hasHitSomething;
        }

        private void OnDrawGizmos()
        {
            if (QuadTiles == null || !DebugDraw)
                return;
            
            Random.InitState(1230321004);
            foreach (var quad in QuadTiles)
            {
                Gizmos.color = Random.ColorHSV(0.0f, 1);
                float r = 0.01f;
                float lerpAmount = 0.1f;
                
                var p0 = Vector3.Lerp(quad.P0, quad.ApproxCentroid, lerpAmount);
                var p1 = Vector3.Lerp(quad.P1, quad.ApproxCentroid, lerpAmount);
                var p2 = Vector3.Lerp(quad.P2, quad.ApproxCentroid, lerpAmount);
                var p3 = Vector3.Lerp(quad.P3, quad.ApproxCentroid, lerpAmount);
                
                Gizmos.DrawSphere(p1, r);
                Gizmos.DrawSphere(p2, r);
                Gizmos.DrawSphere(p3, r);
                
                Gizmos.DrawLine(p0, p1);
                Gizmos.DrawLine(p1, p2);
                Gizmos.DrawLine(p2, p3);
                Gizmos.DrawLine(p3, p0);
                
                Gizmos.DrawSphere(quad.ApproxCentroid, r*2);
            }
        }
    }
}