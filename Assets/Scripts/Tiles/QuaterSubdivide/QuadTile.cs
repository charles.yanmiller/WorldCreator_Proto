using System;
using UnityEngine;
using Math = UnityEngine.ProBuilder.ProBuilderMesh;

namespace WorldCreator.Tiles
{
    [Serializable]
    public struct QuadTile
    {
        public Vector3 P0;
        public Vector3 P1;
        public Vector3 P2;
        public Vector3 P3;
        public Vector3 ApproxCentroid;
        
        public QuadTile(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            P0 = p0;
            P1 = p1;
            P2 = p2;
            P3 = p3;
            //todo this isn't an actually centroid, you need to do a barycentric coordinate thing here for it to be correct.
            ApproxCentroid = (P0 + P1 + P2 + P3) / 4f;
        }

        public struct Hit
        {
            public float Distance;
            public Vector3 Position;


            public Hit(Ray ray, float distance)
            {
                Distance = distance;
                Position = ray.GetPoint(distance);

            }
        }
        public bool Intersect(Ray ray, out Hit hit)
        {
            if (UnityEngine.ProBuilder.Math.RayIntersectsTriangle(ray, P0, P1, P2, out float dist1, out Vector3 point1))
            {
                hit = new Hit(ray, dist1);
                return true;
            }
            
            if (UnityEngine.ProBuilder.Math.RayIntersectsTriangle(ray, P2, P3, P0, out float dist2, out Vector3 point2))
            {
                hit = new Hit(ray, dist2);
                return true;
            }

            hit = new Hit();
            return false;
        }
        
        

    }
}