using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

namespace WorldCreator.Tiles
{
    public class TilesMeshGenerator : MonoBehaviour
    {
        public Centroid [] Centroids;
        public int CentroidNumber = 64;


        public Boundary [] Boundaries;
        public int BoundariesPerCentroid = 3;
        
        public ComputeShader VeronoiCompute;
        public ComputeBuffer CentroidsBuffer;
        public ComputeBuffer BoundariesBuffer;
        private const string KERNAL_NAME = "Main";

        //todo skip voronoi image
        //todo go straight to creating verts from centroids by just marching out from centroid in all directions
        //todo return a list of verts (id to centroid parent, id to opposing neighbor centroid, position)
        //todo debug: visualize these shapes with Shapes!
        //todo mesh this tiles
        //todo texture (in another class), create adjacency list with convenient interfaces...

        private void Start()
        {
            unsafe
            {
                if (!VeronoiCompute.HasKernel(KERNAL_NAME))
                {
                    Debug.LogError($"Could not find kernal: {KERNAL_NAME}");
                    return;
                }


                int kernalIndex = VeronoiCompute.FindKernel(KERNAL_NAME);
                
                CentroidsBuffer = new ComputeBuffer(Centroids.Length, sizeof(Centroid), ComputeBufferType.Structured);
                CentroidsBuffer.SetData(Centroids);
                VeronoiCompute.SetBuffer(kernalIndex, "Centroids", CentroidsBuffer);
                VeronoiCompute.SetInt("CentroidsLength", Centroids.Length);

                BoundariesBuffer = new ComputeBuffer(Boundaries.Length, sizeof(Boundary), ComputeBufferType.Structured);
                BoundariesBuffer.SetData(Boundaries);
                VeronoiCompute.SetBuffer(kernalIndex, "Boundaries", BoundariesBuffer);
                VeronoiCompute.SetInt("BoundariesPerCentroid", BoundariesPerCentroid);
                
                VeronoiCompute.GetKernelThreadGroupSizes(kernalIndex, out uint xGroup, out uint _, out uint __);
                VeronoiCompute.Dispatch(kernalIndex, Centroids.Length/ (int)xGroup, 1, 1);
                CentroidsBuffer.GetData(Centroids);
                BoundariesBuffer.GetData(Boundaries);
            }
        }

        //todo get tangent, 
        //todo rotate tangent as needed
        //todo use rotated tangent as axis to rotate normals to test stuff. 
        private float3 GetTangentToUnitSphere(float3 normal)
        {
            quaternion deltaRotation = quaternion.AxisAngle(new float3(1,0,0), math.PI * 0.001f);
            float3 normalDelta1 = math.normalize(math.rotate(deltaRotation, normal));
            if (math.abs(math.dot(normalDelta1, normal)) < float.Epsilon * 10)
            {
                Debug.LogWarning($"normalDelta1: {normal.ToString()} and deltaNormal: {normalDelta1.ToString()} to close for viable tagent!");
                //todo add fallback here rotating by something else in axis angle
            }

            float3 tangent = normalDelta1 - normal;
            return tangent;
        }

        private void OnDestroy()
        {
            CentroidsBuffer.Dispose();
            BoundariesBuffer.Dispose();
        }

        private void OnDrawGizmos()
        {
            if (Centroids == null)
                return;
            Random.InitState(2300);
            foreach (var centroid in Centroids)
            {
                Random.ColorHSV(0f, 1f);
                Gizmos.color = Random.ColorHSV(0, 1);

                Gizmos.DrawLine(Vector3.zero, centroid.Normal.xyz);
                Gizmos.DrawSphere(centroid.Normal.xyz, 0.02f);
            }
        }

        [ContextMenu(nameof(CreateVeronoiCentroids))]
        public void CreateVeronoiCentroids()
        {
            Centroids = new Centroid[CentroidNumber];
            Boundaries = new Boundary[Centroids.Length * BoundariesPerCentroid];
            for (int i = 0; i < CentroidNumber; i++)
            {
                float3 normalDir = Random.insideUnitSphere;
                float3 tangent = GetTangentToUnitSphere(normalDir);
                Centroids[i] = new Centroid()
                {
                    Normal = new float4(normalDir,0),
                    Tangent = new float4(tangent,0),
                };
                
                for (int j = 0; j < BoundariesPerCentroid; j++)
                {
                    int index = i * BoundariesPerCentroid + j;
                    Boundaries[index] = new Boundary()
                    {
                        CentroidIndex = i,
                        CentroidNeighborIndex = -1,
                    };
                }
            }
        }

        [Serializable]
        [GenerateHLSL(PackingRules.Exact, false)]
        public struct Centroid
        {
            public float4 Normal;
            public float4 Tangent;
        }
        
        [Serializable]
        [GenerateHLSL(PackingRules.Exact, false)]
        public struct Boundary
        {
            public int CentroidIndex;
            public int CentroidNeighborIndex;
            public Vector4 Position;
        }
    }
}