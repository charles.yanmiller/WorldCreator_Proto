//
// This file was automatically generated. Please don't edit by hand. Execute Editor command [ Edit > Rendering > Generate Shader Includes ] instead
//

#ifndef TILESMESHGENERATOR_CS_HLSL
#define TILESMESHGENERATOR_CS_HLSL
// Generated from WorldCreator.Grid.TilesMeshGenerator+Centroid
// PackingRules = Exact
struct Centroid
{
    float4 Normal; // x: x y: y z: z w: w 
    float4 Tangent; // x: x y: y z: z w: w 
};

// Generated from WorldCreator.Grid.TilesMeshGenerator+Boundary
// PackingRules = Exact
struct Boundary
{
    int CentroidIndex;
    int CentroidNeighborIndex;
    float4 Position;
};


#endif
