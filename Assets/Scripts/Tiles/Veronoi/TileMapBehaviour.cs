using System;
using UnityEngine;
using WorldCreator.Utility;

namespace WorldCreator.Tiles
{
    public class TileMapBehaviour : MonoBehaviour
    {
        [ShouldNotBeNull] public Camera Camera;
        public TileMap TileMap;
    }

    [Serializable]
    public class TileMap : MonoBehaviour
    {
        public Vector2Int Dimensions = Vector2Int.one * 10;
        public float HexSize = 1;

        public void CreateHex()
        {

        }

        //todo get me poloygon at this in the world
        //todo get me neighbors of this index of polygon
    }

    public class Cell
    {

    }
}
