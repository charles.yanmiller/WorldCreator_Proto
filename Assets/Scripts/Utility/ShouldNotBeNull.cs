using UnityEditor;
using UnityEngine;

namespace WorldCreator.Utility
{ public class ShouldNotBeNull : PropertyAttribute { }
    [CustomPropertyDrawer(typeof(ShouldNotBeNull))]
    public class ShouldNotBeNullDrawer : PropertyDrawer
    {
        public static Color WARNING_TINT = new Color(1f, 0.74f, 0.56f);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.objectReferenceValue != null)
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }
            label.tooltip = $"{property.displayName} should not be null.\n" + label.tooltip;
            var color = GUI.backgroundColor;
            GUI.color = WARNING_TINT;
            EditorGUI.PropertyField(position, property, label);
            GUI.color = color;
        }
    }
}